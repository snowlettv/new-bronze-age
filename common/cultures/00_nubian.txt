﻿nubian = {
	color = hsv { 0.07  0.88  0.21 }
	primary = light_infantry
	second = camels
	flank = archers

	primary_navy = tetrere
	secondary_navy = octere
	flank_navy = liburnian

	levy_template = levy_kush

	male_names = {
		Amyrteos Ankhmachis Anemhor Charias Djedhor Ezena Horemakhet Khabbabash Lydiadas
 		Nakhthoreb Naravas Nefaarud Neferibre Nesisti Nicias Oezalces Oxyntas Pedubast 
	 Psherenamun Psherenptah Psammuthes Therapon Timoleon Zeteres Zosimus Ahmes
		Ahmose Alarus Amasus Ameny Bakenrenef Harsiotef Henut Ibi Khabash Lagus Nectanebo Kheperkare Neferkare Nehi Osorkon Pami
		Petubastis
	}
	female_names = {
		Agape Agathoclea Amanishabheto Apame Aristomache Aspacia Charis Cynna
		Demetria Elpis Eirene Eudocia Eunice Euphemia Eurydice Euthalia Hypatia Nysa Oenanthe Sostrate Stratonice Thais
	 Thetima Thorakis Timo Lysandra Kiya Qelhata
	}
	family = {		
		Ahmid.Ahmid.Ahmid.Ahmid
		Galestus.Galestid.Galestid.Galestid
		Kenamus.Kenamid.Kenamid.Kenamid
		Naravid.Naravid.Naravid.Naravid
		Penamus.Penamid.Penamid.Penamid
		Senuid.Senuid.Senuid.Senuid
		Setnid.Setnid.Setnid.Setnid
		Arganid.Arganid.Arganid.Arganid
		Psammid.Psammid.Psammid.Psammid
		Philothid.Philothid.Philothid.Philothid
		Timolid.Timolid.Timolid.Timolid
		Zosid.Zosid.Zosid.Zosid
		Phileid.Phileid.Phileid.Phileid
		Nicodemid.Nicodemid.Nicodemid.Nicodemid
		Nefarid.Nefarid.Nefarid.Nefarid
		Bastid.Bastid.Bastid.Bastid
		Xeneid.Xeneid.Xeneid.Xeneid
		Naravid.Naravid.Naravid.Naravid
		Ezanid.Ezanid.Ezanid.Ezanid
		Ahmid.Ahmid.Ahmid.Ahmid
		Alarus.Alara.Alarid.Alarid
		Harsiotes.Harsiotes.Harsiotid.Harsiotid
		Henus.Hena.Henid.Henid
		Ibus.Iba.Ibid.Ibid
		Khabbash.Khabbash.Khabbashid.Khabbashid	
		Amasus.Amasa.Amasid.Amasid
		Nectanebo.Nectanebo.Nectanebid.Nectanebid
		Petubast.Petubast.Petubastes.Petubastid
		Djedhid.Djedhid.Djedhid.Djedhid
		Tachys.Tachya.Tachyes.Tachyid
		Mago.Mago.Mago.Magid
	}

	culture = {
		nubian = {}
	}
	
	barbarian_names = { kushitic_barb }
	
	graphical_culture = nubian_gfx
	
	ethnicities = {
		10 = nubian
	}
}