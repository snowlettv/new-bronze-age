﻿country_decisions = {

	# Form tarhuntassa
	form_one_tarhuntassa= { 
		
		potential = { 
			num_of_cities >= 1 
			NOT = {
				tag = 1TS
			}
			NOR = { 
				is_endgame_tag_trigger = yes
				is_tier_2_formable_trigger = yes #Tier 2 so that Arzawa or Assuwa league can be a larger formable, depending on disputed history
			}
			capital_scope = { #majority tarhuntassan regions for simplicity
				OR = {
					is_in_region = tarhuntassa_region
					is_in_region = west_taurus_region
					is_in_area = lamiya_area
				}
			}				 
			primary_culture = tarhuntassan #only tarhuntassan culture countries
		}
		
		highlight = { 
			scope:province = { #eponymous province territories or other important territories, one per province
				OR = {
					province_id = 3042
					province_id = 3054
					province_id = 3078
					province_id = 3071
					province_id = 3042
					province_id = 3085
					province_id = 3092
					province_id = 3216
					province_id = 3228
					province_id = 3235
					province_id = 3240
					province_id = 3250
					province_id = 3346
					province_id = 3042
					province_id = 3270
					province_id = 3262
					province_id = 3276
					province_id = 3299
					province_id = 3280
				}
			}
		}
		
		allow = { 
			custom_tooltip = {
				text = formable_new_nation_exists
				NOT = {
					any_country = {
						tag = 1TS
					}
				}
			}
			can_form_nation_trigger = yes 
			owns_or_subject_owns = 3042
			owns_or_subject_owns = 3054
			owns_or_subject_owns = 3078
			owns_or_subject_owns = 3071
			owns_or_subject_owns = 3042
			owns_or_subject_owns = 3085
			owns_or_subject_owns = 3092
			owns_or_subject_owns = 3216
			owns_or_subject_owns = 3228
			owns_or_subject_owns = 3235
			owns_or_subject_owns = 3240
			owns_or_subject_owns = 3250
			owns_or_subject_owns = 3346
			owns_or_subject_owns = 3042
			owns_or_subject_owns = 3270
			owns_or_subject_owns = 3262
			owns_or_subject_owns = 3276
			owns_or_subject_owns = 3299
			owns_or_subject_owns = 3280
		}
		
		ai_allow = { #Doesn't have to be owned directly, vassal owning it is fine
			owns_or_subject_owns = 3042
			owns_or_subject_owns = 3054
			owns_or_subject_owns = 3078
			owns_or_subject_owns = 3071
			owns_or_subject_owns = 3042
			owns_or_subject_owns = 3085
			owns_or_subject_owns = 3092
			owns_or_subject_owns = 3216
			owns_or_subject_owns = 3228
			owns_or_subject_owns = 3235
			owns_or_subject_owns = 3240
			owns_or_subject_owns = 3250
			owns_or_subject_owns = 3346
			owns_or_subject_owns = 3042
			owns_or_subject_owns = 3270
			owns_or_subject_owns = 3262
			owns_or_subject_owns = 3276
			owns_or_subject_owns = 3299
			owns_or_subject_owns = 3280
		}
		
		effect = {
			change_country_name = "ONE_TARHUNTASSA_NAME"
			add_5_free_province_investments = yes
			capital_scope = {
				if = {
					limit = {
						root = {
							is_tribal = yes
						}
					}
					capital_formable_tribal_effect = yes
				}
				else = {
					capital_formable_medium_effect = yes
				}
				formable_capital_modifier_normal_effect = yes
			}
			hidden_effect = {
				change_country_adjective = "ONE_TARHUNTASSA_ADJECTIVE"
				change_country_tag = 1TS
				change_country_color = lighter_dark_orange  #Placeholder until a better idea is put forth
				#change_country_flag = Tarhuntassa_FLAG #Haven't a clue what kind of flag this would use
				every_province = {
					limit = {
						OR = {
							is_in_region = tarhuntassa_region
							is_in_region = west_taurus_region
							is_in_area = lamiya_area
						}
						NOT = { owner = ROOT }
					}
					add_claim = ROOT 
				}
			}
		}

		ai_will_do = {
			base = 1
		}
	}
} 