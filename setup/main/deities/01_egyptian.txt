deity_manager = {
	deities_database = { ### KEYS 200-399 ###
		# Egyptian Deities 200-399
		200 = {
			key = omen_ptah
			deity = deity_ptah
		}
		201 = {
			key = omen_hathor
			deity = deity_hathor
		}
		202 = {
			key = omen_neith
			deity = deity_neith
		}
		203 = {
			key = omen_wadjet
			deity = deity_wadjet
		}
		204 = {
			key = omen_amun
			deity = deity_amun
		}
		205 = {
			key = omen_montu
			deity = deity_montu
		}
		206 = {
			key = omen_sobek
			deity = deity_sobek
		}
		207 = {
			key = omen_set
			deity = deity_set
		}
		208 = {
			key = omen_min
			deity = deity_min
		}
		209 = {
			key = omen_thoth
			deity = deity_thoth
		}
		210 = {
			key = omen_anubis
			deity = deity_anubis
		}
		211 = {
			key = omen_sekhmet
			deity = deity_sekhmet
		}
		212 = {
			key = omen_ra
			deity = deity_ra
		}
		213 = {
			key = omen_bastet
			deity = deity_bastet
		}
		214 = {
			key = omen_khnum
			deity = deity_khnum
		}
		215 = {
			key = omen_horus
			deity = deity_horus
		}
		216 = {
			key = omen_nekhbet
			deity = deity_nekhbet
		}
		217 = {
			key = omen_osiris
			deity = deity_osiris
		}
		218 = {
			key = omen_repyt
			deity = deity_repyt
		}
		219 = {
			key = omen_anhur
			deity = deity_anhur
		}
		220 = {
			key = omen_nemty
			deity = deity_nemty
		}
		221 = {
			key = omen_wepwawet
			deity = deity_wepwawet
		}
		222 = {
			key = omen_heryshaf
			deity = deity_heryshaf
		}
		223 = {
			key = omen_bat
			deity = deity_bat
		}
	}
}

