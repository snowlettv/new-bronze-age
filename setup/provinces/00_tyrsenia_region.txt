﻿2175={ #Maroneia
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=13
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2176={ #Mesembria
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2177={ #Sale
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wild_game"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2178={ #Apalos
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fur"
	civilization_value=13
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2179={ #Doriko
	terrain="deep_forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2180={ #Pylaia
	terrain="deep_forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="copper"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2181={ #Loutros
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2182={ #Doriskos
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2183={ #Ainos
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2184={ #Reniso
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2185={ #Vrenviso
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2186={ #Raphikia
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="leather"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2187={ #Magarisi
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=13
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=2
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=2
	}
}

2188={ #Brago
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2189={ #Pigi
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2190={ #Adele
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="salt"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2192={ #Sphakaki
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="earthware"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2193={ #Irini
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2194={ #Triado
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="hemp"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2196={ #Germene
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=3
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2197={ #Latonike
	terrain="deep_forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wild_game"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2198={ #Thyarei
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="horses"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2199={ #Hyrko
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2200={ #Praigon
	terrain="deep_forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="base_metals"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2227={ #Demetrion
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cloth"
	civilization_value=15
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

2228={ #Tityasi
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2229={ #Adadi
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2230={ #Modri
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2231={ #Tarsia
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fur"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2232={ #Kabia
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2233={ #Imbros
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2234={ #Poliochne
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=6
	}
	freemen={
		amount=5
	}
	slaves={
		amount=2
	}
	holy_site = omen_hephaistos
}

2235={ #Hephaistas
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fur"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2236={ #Apsoda
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fruits"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=5
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2237={ #Lagania
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

2238={ #Myrina
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2450={ #Elaious
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2451={ #Madytos
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2452={ #Koila
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2453={ #Etenne
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2454={ #Drabos
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="grain"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2455={ #Sestos
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2457={ #Pogla
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2458={ #Kremna
	terrain="mountain_valley"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="base_metals"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=3
	}
	tribesmen={
		amount=1
	}
}

2459={ #Aigos
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

2462={ #Hulaiesi
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

2463={ #Mimulu
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=6
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2465={ #Aphrodisias
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="olive"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2466={ #Sivai
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=24
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2467={ #Larissale
	terrain="farmland"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wine"
	civilization_value=16
	barbarian_power=0
	province_rank="city"
	citizen={
		amount=3
	}
	freemen={
		amount=4
	}
	slaves={
		amount=1
	}
	tribesmen={
		amount=1
	}
}

2470={ #Murinail
	terrain="mountain_valley"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=2
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=4
	}
	tribesmen={
		amount=1
	}
}

2487={ #Mati
	terrain="plains"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fish"
	civilization_value=5
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2585={ #Rousion
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2586={ #Modra
	terrain="forest"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wild_game"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2587={ #Kierai
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=2
	}
	tribesmen={
		amount=2
	}
}

2588={ #Taitame
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="fur"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2589={ #Aproi
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="wood"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}

2590={ #Tarsiai
	terrain="hills"
	culture="tyrsenian"
	religion="aegean_religion"
	trade_goods="cattle"
	civilization_value=3
	barbarian_power=0
	province_rank="settlement"
	freemen={
		amount=1
	}
	tribesmen={
		amount=3
	}
}