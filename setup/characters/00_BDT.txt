﻿"BDT"={
	country="BDT"
	4={
		first_name="Ankhtifi"
		family_name="Thety-Horus"
		birth_date=748.4.25
		culture="upper_egyptian"
		religion="egyptian_religion"
		no_stats=yes
		add_martial=11
		add_charisma=8
		add_finesse=7
		add_zeal=5
		no_traits=yes
		add_trait="generous"
		add_trait="blunt"
		add_trait="just"
		add_gold=300
		add_popularity=90
		c:BDT={
			set_as_ruler=char:4
		}
		dna="BPME8x+IH4ju8O7wAokCiQKPAo8DnwOfAWABYAJ4AngBUgFSApgCmAKEAoQDmwObAoQChAKBAoECkAKQAmgCaAPFA8UCfgJ+AmoCagJwAnACRAJEAr8CvwKYApgDmwObAokCiQPaA9oCcQJxAoUChQOhA6ECbgJuAnsCewKTApMCcwJzAmYCZgKAAoACmQKZA5sDmwJwAnAClAKUApsCmwKMAowCeQJ5AnwCfAKBAoEBVgFWAmYCZgGZAZkCjAKMAosCiwJWAlYCzALMAnwCfALJAskDlgOWAnUCdQKXApcCiAKIAmgCaAILAgsAAgACACgAKAFCAUIAHwAfAAAAAADdAN0BDAEMAV8BXwAwADABdgF2AFcAVwB/AH8BRgFGAHkAeQLSAtIAKQApAFMAUwCOAI4BzAHMAu8C7wB/AH8BVwFXAFEAUQFLAUsBLgEuAWoBagCYAJgAGgAaAIUAhQBbAFsDAAMAAu4C7gMfAx8CzgViAH8AfwIAAgAPtg+8AAMAAweVB5UI8QjxChAKEAAAAAAAAAAAAAAAAAAAAAA="
	}
}
